
///httpcore-4.2.2
///httpclient-4.2.2
///httpmime-4.2.2
///commons-io-2.4
///commons-logging-1.1.1

import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class ClientMultipartFormPost 
{
    public static void main(String[] args) throws Exception 
    {
        if (args.length != 1)  
        {
            System.out.println("File path not given");
            System.exit(1);
        }
        HttpClient httpclient = new DefaultHttpClient();
        try 
        {
            HttpPost httppost = new HttpPost("http://localhost:7070");	//http://server_address:port

            FileBody bin = new FileBody(new File(args[0]));
            
            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("bin", bin);
           
            httppost.setEntity(reqEntity);

            System.out.println("executing request " + httppost.getRequestLine());
            System.out.println("content length: " + reqEntity.getContentLength());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();

            System.out.println("\n"+response.getStatusLine());
            if (resEntity != null) 
            {
                InputStream instream = resEntity.getContent();	
                instream.read();
                StringWriter writer = new StringWriter();
                IOUtils.copy(instream, writer);
                System.out.println("Response: "+writer.toString());
            }
            EntityUtils.consume(resEntity);
        } finally 
        {
            try 
            { 
            	httpclient.getConnectionManager().shutdown(); 
            } catch (Exception ignore) {}
        }
    }
}
